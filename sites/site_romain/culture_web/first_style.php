<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/style.css">
    <meta charset="utf-8">
    <title></title>
  </head>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script>
  $(document).ready(function(){
    $("#commande").toggle();
    $(".buttonGit").click(function(){
        $("#commande").toggle();
    });
});
</script>
  <body>
    <h1> Site ressource en Html & CSS</h1>
    <h2 class="buttonGit">Utilisation & commande du Git</h2>
    <pre id="commande">
  #UTILISATION

- Ouvrir le Terminal ( on regarde si il y a git )
- faire la commande Git help
- nous verrons si nous avons git ou pas
- pour ceux qui n’ont pas Git
- On va sur : https://git-scm.com/
- On télécharge Git
- click sur dowload manualy
- on arrive sur source Forge
- on ne double click pas sur universal maverick.pkg
- click droit « ouvrir » + ouvrir
- instalation du logiciel
- on retourne dans le terminal vérifier git help
- pour ceux qui ont windows on va utiliser cmdr
-  https://www.youtube.com/watch?v=chhVBZfRFgI&list=PLjwdMgw5TTLXuY5i7RW0QqGdW0NZntqiP&index=5
- cliquer en bas sur
- aller dans le dossier en ligne de commande d’abord avec cd aller au bon endroit.

# COMMANDES
# Cloner un projet
git clone [url]

# Initialiser un dépôt
git init

# Suivre des fichiers ou les ajouter à la staging area
git add fichier1 fichier2

# retrancher un fichier du staging (défaire le git add)
git reset HEAD fichier

# Voir le statut
git status

# Visualiser ce qui a été modifié mais pas encore indexé
git diff

# Demander à git d'afficher les diff des fichiers ajouté à la staging (avec git add)
git diff --staged

# Visualiser les modifications indexées qui feront partie de la prochaine validation
git diff --staged

# Enregistrer "commiter" les modifs
git commit

# Commiter sans passer par l'éditeur de texte
git commit -m "message de commit"

# Tout comiter sans passer par add
git commit -a

# Placer un tag (lightweight)
git tag 1.2.0

# Placer un tag sur un commit antérieur
git tag 1.1.4 ffaae76

# push n'envoie pas les tags par défaut
# pour pusher les tags
git push --tags

# Voir l'historique des commits (l'option --all permet de voir l'ensemble des branches)
# il est possible d'appeler l'option --graph pour rendre ça plus visuel
# on peut préciser un fichier pour n'obtenir que les commits relatifs à ce dernier
git log [--all]
</pre>
    <ul class="projet">
            <p>PROJETS</p>
            <li class="element"><a href="B2_75/clothilde"><p>Rebecca ⤦</p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>
            <li class="element"><a href="B2_75/Deep_web"><p>François ⤦</p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>
            <li class="element"><a href="B2_75/clothilde"><p>Waine ⤦</p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>
            <li class="element"><a href="B2_75/clothilde"><p>Marie-Pierre ⤦</p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>
            <li class="element"><a href="B2_75/clothilde"><p>Aurore ⤦ </p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>
            <li class="element"><a href="B2_75/clothilde"><p>Maelan ⤦ </p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>
            <li class="element"><a href="B2_75/clothilde"><p>Louise ⤦</p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>
            <li class="element"><a href="B2_75/clothilde"><p>Benoît ⤦</p><iframe src="B2_75/clothilde" width="100%" height="100%"></iframe></a></li>

      </ul>
    </body>
</html>
